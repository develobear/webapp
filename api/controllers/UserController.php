<?php

namespace api\controllers;

class UserController extends \yii\rest\ActiveController
{
    public $modelClass = 'api\models\ApiUser';
}
