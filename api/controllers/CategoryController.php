<?php

namespace api\controllers;

class CategoryController extends \yii\rest\ActiveController
{
    public $modelClass = 'api\models\ApiCategory';
}
