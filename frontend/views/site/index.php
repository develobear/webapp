<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">

            <div class="page-header">
                <h1>User routes</h1>
            </div>
            <div id="main" data-url="http://api.webapp.localhost/"></div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="row">
                            <div id="user" data-url="users/"></div>
                            <div class="btn-group-custom-lg btn-group-vertical" role="group" aria-label="...">
                                <div class="btn btn-default btn-send" data-method="get" >
                                    GET /users
                                </div>
                                <div class="btn btn-default btn-send" data-method="head">
                                    HEAD /users
                                </div>
                                <div class="btn btn-default btn-send" data-method="post">
                                    POST /users
                                </div>
                                <div class="btn btn-default btn-send" data-method="options">
                                    OPTIONS /users
                                </div>
                            </div>
                            <hr>
                            <div class="input-block">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="123">
                                    <div class="btn btn-default btn-send" data-method="get"">GET /users</div>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="123">
                                    <div class="btn btn-default" data-method="head">HEAD /users</div>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="123">
                                    <div class="btn btn-default btn-send" data-method="post">POST /users</div>
                                </div>
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="123">
                                    <div class="btn btn-default" data-method="patch" >PATCH|PUT /users</div>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" data-method="options" type="button">OPTIONS /users</button>
                                    </span>
                                    <input type="text" class="form-control" placeholder="123">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="row">

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
