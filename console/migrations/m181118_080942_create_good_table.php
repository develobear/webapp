<?php

use yii\db\Migration;

/**
 * Handles the creation of table `good`.
 */
class m181118_080942_create_good_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('good', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string(255),
            'price' => $this->float(),
            'description' => $this->text(),
            'photo' => $this->string(255),
            'photo_description' => $this->text()
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            'idx-good-category_id',
            'good',
            'category_id'
        );

        // add foreign key for table `category`
        $this->addForeignKey(
            'idx-good-category_id',
            'good',
            'category_id',
            'category',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('good');
    }
}
