<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_good`.
 */
class m181118_081304_create_user_good_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('user_good', [
            'user_id' => $this->integer(),
            'good_id' => $this->integer(),
            'count' => $this->integer(),
            'created_at' => $this->dateTime(),
            'PRIMARY KEY(user_id, good_id)'
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_good-user_id',
            'user_good',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_good-user_id',
            'user_good',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `good_id`
        $this->createIndex(
            'idx-user_good-good_id',
            'user_good',
            'good_id'
        );

        // add foreign key for table `good`
        $this->addForeignKey(
            'fk-user_good-good_id',
            'user_good',
            'good_id',
            'good',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_good-user_id',
            'user_good'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'ind-user_good-user_id',
            'user_good'
        );

        // drops foreign key for table `good`
        $this->dropForeignKey(
            'fk-user_good-good_id',
            'user_good'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            'idx-user_good-good_id',
            'user_good'
        );

        $this->dropTable('user_good');
    }
}
