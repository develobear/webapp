<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m181118_080956_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'name' => $this->string(255),
            'description' => $this->text()
        ]);

        // creates index for column `parent_id`
        $this->createIndex(
            'idx-category-parent_id',
            'category',
            'parent_id'
        );

        // add foreign key for table `category`
        $this->addForeignKey(
            'fk-category-parent_id',
            'category',
            'parent_id',
            'category',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        // drops foreign key for table `category`
        $this->dropForeignKey(
            'fk-category-parent_id',
            'category'
        );

        // drops index for column `parent_id`
        $this->dropIndex(
            'ind-category-parent_id',
            'category'
        );


        $this->dropTable('category');
    }
}
