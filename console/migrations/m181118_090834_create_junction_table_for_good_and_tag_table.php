<?php

use yii\db\Migration;

/**
 * Handles the creation of table `good_tag`.
 * Has foreign keys to the tables:
 *
 * - `good`
 * - `tag`
 */
class m181118_090834_create_junction_table_for_good_and_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('good_tag', [
            'good_id' => $this->integer(),
            'tag_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'PRIMARY KEY(good_id, tag_id)',
        ]);

        // creates index for column `good_id`
        $this->createIndex(
            'idx-good_tag-good_id',
            'good_tag',
            'good_id'
        );

        // add foreign key for table `good`
        $this->addForeignKey(
            'fk-good_tag-good_id',
            'good_tag',
            'good_id',
            'good',
            'id',
            'CASCADE'
        );

        // creates index for column `tag_id`
        $this->createIndex(
            'idx-good_tag-tag_id',
            'good_tag',
            'tag_id'
        );

        // add foreign key for table `tag`
        $this->addForeignKey(
            'fk-good_tag-tag_id',
            'good_tag',
            'tag_id',
            'tag',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        // drops foreign key for table `good`
        $this->dropForeignKey(
            'fk-good_tag-good_id',
            'good_tag'
        );

        // drops index for column `good_id`
        $this->dropIndex(
            'idx-good_tag-good_id',
            'good_tag'
        );

        // drops foreign key for table `tag`
        $this->dropForeignKey(
            'fk-good_tag-tag_id',
            'good_tag'
        );

        // drops index for column `tag_id`
        $this->dropIndex(
            'idx-good_tag-tag_id',
            'good_tag'
        );

        $this->dropTable('good_tag');
    }
}
